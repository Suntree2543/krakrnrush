using System.Collections.Generic;
using Facebook.Unity;
using TMPro;
using UnityEngine;


    public class fbController : MonoBehaviour
    {
        // Start is called before the first frame update
        public TextMeshProUGUI testlogged;

        void Awake()
        {
            // FB.Init(SetInit,OnHideUnity);
            if (!FB.IsInitialized)
            {
                // Initialize the Facebook SDK
                FB.Init(InitCallback, OnHideUnity);
            }
            else
            {
                // Already initialized, signal an app activation App Event
                FB.ActivateApp();
            }
        }

        // void SetInit()
        // {
        //     if (FB.IsLoggedIn)
        //     {
        //         Debug.Log("Logged in");
        //     }
        //     else
        //     {
        //         Debug.Log("fail");
        //     }
        // }
        private void InitCallback()
        {
            if (FB.IsInitialized)
            {
                // Signal an app activation App Event
                FB.ActivateApp();
                // Continue with Facebook SDK
                // ...
            }
            else
            {
                Debug.Log("Failed to Initialize the Facebook SDK");
            }
        }

        void OnHideUnity(bool isGameShown)
        {
            if (!isGameShown)
            {
                // Pause the game - we will need to hide
                Time.timeScale = 0;
            }
            else
            {
                // Resume the game - we're getting focus again
                Time.timeScale = 1;
            }
        }

        public void FBLogin()
        {
            // List<string> permissions = new List<string>();
            // permissions.Add("test");
            // FB.LogInWithPublishPermissions(permissions,AunthCallResualt);
            var perms = new List<string>() {"public_profile", "email"};
            FB.LogInWithReadPermissions(perms);
        }

        // private void AuthCallback(ILoginResult result)
        // {
        //     if (FB.IsLoggedIn)
        //     {
        //         // AccessToken class will have session details
        //         var aToken = Facebook.Unity.AccessToken.CurrentAccessToken;
        //         // Print current access token's User ID
        //         Debug.Log(aToken.UserId);
        //         // Print current access token's granted permissions
        //         foreach (string perm in aToken.Permissions)
        //         {
        //             Debug.Log(perm);
        //         }
        //     }
        //     else
        //     {
        //         Debug.Log("User cancelled login");
        //     }
        // }

        // void AunthCallResualt(ILoginResult result)
        // {
        //     if (result.Error != null)
        //     {
        //         Debug.Log(result.Error);
        //     }
        //     else
        //     {
        //         if (FB.IsLoggedIn)
        //         {
        //             Debug.Log("logged in");
        //             FB.API("test",HttpMethod.GET,callbackData);
        //             //Debug.Log(result.RawResult);
        //         }
        //         else
        //         {
        //             Debug.Log("fail");
        //         }
        //     }
        // }


    }

