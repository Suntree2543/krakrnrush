using System.Collections;
using System.Collections.Generic;
using Doozy.Runtime.UIManager.Containers;
using UnityEngine;
using UnityEngine.Purchasing;

public class IAPManager : MonoBehaviour
{

   public void OnPurachaseComplete(Product product)
   {
      //Star
      if (product.definition.id == "25star")
      {
         Debug.Log("25 Star");
         
         //Popup
         var popup = UIPopup.Get("25Star");
         if (popup == null) return;
         //popup.Show();
         
         //increaseMoneystat
         Player.player.playerData.star += 25;
         Player.player.playerData.star =  Player.player.playerData.star;
      }
      if (product.definition.id == "55star")
      {
         Debug.Log("55 Star");
         
         //Popup
         // var popup = UIPopup.Get("25Star");
         // if (popup == null) return;
         //popup.Show();
         
         //increaseMoneystat
         Player.player.playerData.star += 55;
         Player.player.playerData.star =  Player.player.playerData.star;
      }
      if (product.definition.id == "125star")
      {
         Debug.Log("125 Star");
         
         //Popup
         // var popup = UIPopup.Get("25Star");
         // if (popup == null) return;
         //popup.Show();
         
         //increaseMoneystat
         Player.player.playerData.star+= 125;
         Player.player.playerData.star = Player.player.playerData.star;
      }
      if (product.definition.id == "350star")
      {
         Debug.Log("350 Star");
         
         //Popup
         // var popup = UIPopup.Get("25Star");
         // if (popup == null) return;
         //popup.Show();
         
         //increaseMoneystat
         Player.player.playerData.star += 350;
         Player.player.playerData.star = Player.player.playerData.star;
      }
      if (product.definition.id == "800star")
      {
         Debug.Log("800 Star");
         
         //Popup
         // var popup = UIPopup.Get("25Star");
         // if (popup == null) return;
         //popup.Show();
         
         //increaseMoneystat
         Player.player.playerData.star += 800;
         Player.player.playerData.star = Player.player.playerData.star;
      }
       
      //Coin
      if (product.definition.id == "7500coin")
      {
         Debug.Log("7500coin");
         
         //Popup
         // var popup = UIPopup.Get("25Star");
         // if (popup == null) return;
         //popup.Show();
         
         //increaseMoneystat
         Player.player.playerData.coin += 7500;
         Player.player.playerData.coin =  Player.player.playerData.coin;
      }
      if (product.definition.id == "40000coin")
      {
         Debug.Log("40000coin");
         
         //Popup
         // var popup = UIPopup.Get("25Star");
         // if (popup == null) return;
         //popup.Show();
         
         //increaseMoneystat
         Player.player.playerData.coin += 40000;
         Player.player.playerData.coin =  Player.player.playerData.coin;
      }
      if (product.definition.id == "90000coin")
      {
         Debug.Log("90000coin");
         
         //Popup
         // var popup = UIPopup.Get("25Star");
         // if (popup == null) return;
         //popup.Show();
         
         //increaseMoneystat
         Player.player.playerData.coin += 90000;
         Player.player.playerData.coin =  Player.player.playerData.coin;
      }
      
      if (product.definition.id == "200000coin")
      {
         Debug.Log("200000coin");
         
         //Popup
         // var popup = UIPopup.Get("25Star");
         // if (popup == null) return;
         //popup.Show();
         
         //increaseMoneystat
         Player.player.playerData.coin += 200000;
         Player.player.playerData.coin =  Player.player.playerData.coin;
      }
      if (product.definition.id == "550000coin")
      {
         Debug.Log("550000coin");
         
         //Popup
         // var popup = UIPopup.Get("25Star");
         // if (popup == null) return;
         //popup.Show();
         
         //increaseMoneystat
         Player.player.playerData.coin += 550000;
         Player.player.playerData.coin =  Player.player.playerData.coin;
      }
      
   }

   public void OmPuarchaseFailed(Product product, PurchaseFailureReason failureReason)
   {
      Debug.Log(product.definition.id + "Failed" + failureReason);
   }
}
